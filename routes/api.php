<?php

use App\Http\Controllers\API\ADM_DocumentosApiController;
use App\Http\Controllers\API\ADM_PerfilesApiController;
use App\Http\Controllers\API\ADM_PersonasApiController;
use App\Http\Controllers\API\ADM_RolesApiController;
use App\Http\Controllers\API\UsuariosApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:sanctum');


Route::middleware('api')->group(function () {
    Route::apiResource('Usuarios', UsuariosApiController::class);
});


Route::middleware('api')->group(function () {
    Route::apiResource('ADM_Personas', ADM_PersonasApiController::class);
});


Route::middleware('api')->group(function () {
    Route::apiResource('ADM_Perfiles', ADM_PerfilesApiController::class);
});


Route::middleware('api')->group(function () {
    Route::apiResource('ADM_Roles', ADM_RolesApiController::class);
});


Route::middleware('api')->group(function () {
    Route::apiResource('ADM_Documentos', ADM_DocumentosApiController::class);
});
