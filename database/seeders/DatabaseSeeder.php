<?php

namespace Database\Seeders;

use App\Models\ADM_Documentos;
use App\Models\ADM_Perfiles;
use App\Models\ADM_Personas;
use App\Models\ADM_Roles;
use App\Models\User;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Usuarios;
use Illuminate\Database\Seeder;
use Faker\Factory as FakerFactory;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // User::factory(10)->create();
        define('LLENAR_DB',true);
/*
        User::factory()->create([
            'name' => 'Test User',
            'email' => 'test@example.com',
        ]);
*/
        $cantidadInsertar = false;

        if(LLENAR_DB){
            // Seed roles
            $faker = FakerFactory::create();

            for ($i = 1; $i <= ($cantidadInsertar ? : 10); $i++) {
                ADM_Roles::create([
                    'nombre' => $faker->unique()->name,
                    'codigo' => $faker->unique()->text,
                    'activo' => true,
                ]);
            }

            // Seed people
            for ($i = 1; $i <= ($cantidadInsertar ? : 10000); $i++) {
                ADM_Personas::create([
                    'nombre' => $faker->name,
                    'apellido' => $faker->lastName,
                    'sexo' => $faker->randomElement(['M', 'F']),
                    'fecha_nacimiento' => $faker->date('Y-m-d'),
                ]);
            }

            // Seed users (one per person)
            for ($i = 1; $i <= ($cantidadInsertar ? : 10000); $i++) {
                $name = $faker->name;
                $email = $faker->unique()->safeEmail;
                $password = bcrypt('password');

                $persona = ADM_Personas::find($i);

                User::factory()->create([
                    'name' => $name,
                    'email' => $email,
                    'password' => $password,
                ]);

                Usuarios::create([
                    'adm_persona_id' => $persona->id,
                    'user_id' => $persona->id,
                    'name' => $name,
                    'email' => $email,
                    'password' => $password,
                    'activo' => $faker->randomElement([true, false]),
                    'bloqueado' => false,
                ]);
            }

            // Seed documents (N per person, 1 principal)
            for ($i = 1; $i <= ($cantidadInsertar ? : 10000); $i++) {
                $persona = ADM_Personas::find($i);
                $numDocumentos = rand(1, 5); // Generate random number of documents between 1 and 5

                for ($j = 1; $j <= $numDocumentos; $j++) {
                    $esPrincipal = ($j === 1) ? true : false; // Set first document as principal

                    ADM_Documentos::create([
                        'adm_persona_id' => $persona->id,
                        'numero' => $faker->unique()->randomNumber(8),
                        'es_principal' => $esPrincipal,
                    ]);
                }
            }

            // Seed profiles (multiple per user)
            for ($i = 1; $i <= ($cantidadInsertar ? : 10000); $i++) {
                $usuario = Usuarios::find($i);
                $numPerfiles = rand(1, 3); // Generate random number of profiles between 1 and 3

                for ($j = 1; $j <= $numPerfiles; $j++) {
                    $rol = ADM_Roles::inRandomOrder()->first(); // Assign a random role to the profile

                    ADM_Perfiles::create([
                        'adm_rol_id' => $rol->id,
                        'user_id' => $usuario->id,
                        'descripcion' => $faker->sentence,
                    ]);
                }
            }
        }


    }
}
