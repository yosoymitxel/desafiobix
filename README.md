# Desafío Práctico Bix

Este proyecto implementa un sistema de gestión de usuarios y documentos utilizando Laravel, Vue.js y PostgreSQL.

## Características:

* **Autenticación de usuario:**
    * Inicio de sesión con documento principal y contraseña.
    * Validación de contraseña: mínimo 8 caracteres, máximo 16 caracteres, mayúsculas, minúsculas, dígitos y caracteres especiales (*, ., _).
    * Manejo de errores de inicio de sesión incorrecto.
    * Cierre de sesión.
* **Gestión de usuarios:**
    * Listado de usuarios con información básica (nombre, apellido, email, estado, edad, perfiles).
    * Filtros de búsqueda por nombre, apellido y nombre de usuario.
    * Manejo de errores.
* **Gestión de documentos:**
    * Un usuario puede tener N documentos, pero solo uno principal.
    * Creación de documentos.
    * Asignación de documento principal a un usuario.
* **API REST:**
    * Servicio de login (GET).
    * Listado de usuarios (GET).
    * Cambio de contraseña (PUT).
    * Eliminación masiva de usuarios (DELETE).
    * Todos los servicios con formato JSON y autenticación JWT.
* **Pruebas:**
    * Pruebas unitarias de una funcionalidad.
    * Pruebas de API con Postman.
    * Medición de rendimiento de los servicios.

## Tecnologías:

* **Backend:** Laravel 9, PostgreSQL 14
* **Frontend:** Vue.js
* **Control de versiones:** GitLab

## Instrucciones de uso:

1. **Clonar el repositorio:**

```bash
git clone https://gitlab.com/yosoymitxel/desafio2024.git
```

2. **Instalar dependencias:**

```bash
composer install
npm install
```

3. **Configurar base de datos:**

* Crear una base de datos `desafio_bix` en PostgreSQL.
* Configurar el archivo `.env` con la información de conexión a la base de datos.

4. **Ejecutar migraciones y seeders:**

```bash
php artisan migrate
php artisan db:seed
```

5. **Iniciar servidor web:**

```bash
php artisan serve
```

6. **Acceder a la aplicación:**

Abrir un navegador web y navegar a [https://localhost8000.com/](https://localhost8000.com/).

7. **Iniciar sesión:**

Utilizar un documento principal y contraseña ficticios.

8. **Probar funcionalidades:**

* Acceder al listado de usuarios.
* Aplicar filtros de búsqueda.
* Cambiar la contraseña de un usuario.
* Eliminar usuarios de forma masiva.

## Notas:

* Este proyecto es solo para fines educativos.
* Se recomienda revisar el código fuente para comprender mejor la implementación.
* Se pueden agregar nuevas funcionalidades y mejorar el diseño de la aplicación según las necesidades.

## Licencia:

MIT
