<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ADM_Documentos extends Model
{
    protected $table = 'adm_documentos';

    protected $fillable = ['id','adm_persona_id','numero','es_principal'];

    public function persona()
    {
        return $this->belongsTo(ADM_Personas::class, 'adm_persona_id');
    }
}
