<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Usuarios extends Model
{
    protected $table = 'usuarios';

    protected $fillable = ['id','adm_persona_id','user_id','name','email','password','activo','bloqueado'];

    public function persona()
    {
        return $this->belongsTo(ADM_Personas::class, 'adm_persona_id');
    }

    public function perfiles()
    {
        return $this->belongsToMany(ADM_Perfiles::class, 'adm_perfiles_usuarios', 'user_id', 'adm_perfil_id');
    }
}
