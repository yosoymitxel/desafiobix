<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ADM_Roles extends Model
{
    protected $table = 'adm_roles';

    protected $fillable = ['id','nombre','codigo','activo'];

    public function perfiles()
    {
        return $this->hasMany(ADM_Perfiles::class, 'adm_rol_id');
    }

}
