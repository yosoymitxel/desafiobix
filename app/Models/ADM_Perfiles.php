<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ADM_Perfiles extends Model
{
    protected $table = 'adm_perfiles';

    protected $fillable = ['id','adm_rol_id','user_id','activo','descripcion'];

    public function usuario()
    {
        return $this->belongsTo(Usuarios::class, 'user_id');
    }

    public function rol()
    {
        return $this->belongsTo(ADM_Roles::class, 'adm_rol_id');
    }
}
