<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ADM_Personas extends Model
{
    protected $table = 'adm_personas';

    protected $fillable = ['id','nombre','apellido','sexo, [M, F]','fecha_nacimiento'];

    public function usuario()
    {
        return $this->hasOne(Usuarios::class, 'adm_persona_id');
    }

    public function documentos()
    {
        return $this->hasMany(ADM_Documentos::class, 'adm_persona_id');
    }
}
