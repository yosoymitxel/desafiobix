<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Usuarios;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UsuariosApiController extends Controller
{
    //




    public function index()
    {
        $usuarios = Usuarios::all();
        return response()->json($usuarios, 201);
    }



    public function show($id)
    {
        $usuarios = Usuarios::findOrFail($id);
        return response()->json($usuarios, 201);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), array (
            'adm_persona_id' => 'required|foreignId',
            'name' => 'required|string',
            'email' => 'required|string',
            'password' => 'required|string',
            'activo' => 'required|boolean',
            'bloqueado' => 'required|boolean',
        ));

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $data = $request->all();

        $usuarios = Usuarios::create($data);

        return response()->json($usuarios, 201);
    }

    public function calcularEdad()
    {
        if (!$this->fecha_nacimiento) {
            return null;
        }

        $birthDate = new DateTime($this->fecha_nacimiento);
        $today = new DateTime();
        $age = $today->diff($birthDate)->y;

        return $age;
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), array (
            'adm_persona_id' => 'required|foreignId',
            'name' => 'required|string',
            'email' => 'required|string',
            'password' => 'required|string',
            'activo' => 'required|boolean',
            'bloqueado' => 'required|boolean',
        ));

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $data = $request->all();

        $usuarios = Usuarios::findOrFail($id);
        $usuarios->update($data);

        return response()->json($usuarios, 200);
    }



    public function destroy($id)
    {
        Usuarios::destroy($id);
        return response()->json(['message' => 'Registro eliminado correctamente'], 200);
    }
}
