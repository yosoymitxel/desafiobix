<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\ADM_Personas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ADM_PersonasApiController extends Controller
{
    //




    public function index()
    {
        $adm_personas = ADM_Personas::all();
        return response()->json($adm_personas, 201);
    }



    public function show($id)
    {
        $adm_personas = ADM_Personas::findOrFail($id);
        return response()->json($adm_personas, 201);
    }



    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), array (
  'nombre' => 'required|string',
  'apellido' => 'required|string',
  'sexo, [M, F]' => 'required|enum',
  'fecha_nacimiento' => 'required|date',
));

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $data = $request->all();

        $adm_personas = ADM_Personas::create($data);

        return response()->json($adm_personas, 201);
    }



    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), array (
  'nombre' => 'required|string',
  'apellido' => 'required|string',
  'sexo, [M, F]' => 'required|enum',
  'fecha_nacimiento' => 'required|date',
));

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $data = $request->all();

        $adm_personas = ADM_Personas::findOrFail($id);
        $adm_personas->update($data);

        return response()->json($adm_personas, 200);
    }



    public function destroy($id)
    {
        ADM_Personas::findOrFail($id);
        ADM_Personas::destroy($id);
        return response()->json(['message' => 'Registro eliminado correctamente'], 200);
    }
    }
