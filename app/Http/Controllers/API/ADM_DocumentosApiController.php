<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\ADM_Documentos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ADM_DocumentosApiController extends Controller
{
    //




    public function index()
    {
        $adm_documentos = ADM_Documentos::all();
        return response()->json($adm_documentos, 201);
    }



    public function show($id)
    {
        $adm_documentos = ADM_Documentos::findOrFail($id);
        return response()->json($adm_documentos, 201);
    }



    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), array (
  'adm_persona_id' => 'required|foreignId',
  'numero' => 'required|string',
  'es_principal' => 'required|boolean',
));

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $data = $request->all();

        $adm_documentos = ADM_Documentos::create($data);

        return response()->json($adm_documentos, 201);
    }



    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), array (
  'adm_persona_id' => 'required|foreignId',
  'numero' => 'required|string',
  'es_principal' => 'required|boolean',
));

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $data = $request->all();

        $adm_documentos = ADM_Documentos::findOrFail($id);
        $adm_documentos->update($data);

        return response()->json($adm_documentos, 200);
    }



    public function destroy($id)
    {
        ADM_Documentos::findOrFail($id);
        ADM_Documentos::destroy($id);
        return response()->json(['message' => 'Registro eliminado correctamente'], 200);
    }
    }
