<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\ADM_Perfiles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ADM_PerfilesApiController extends Controller
{
    //




    public function index()
    {
        $adm_perfiles = ADM_Perfiles::all();
        return response()->json($adm_perfiles, 201);
    }



    public function show($id)
    {
        $adm_perfiles = ADM_Perfiles::findOrFail($id);
        return response()->json($adm_perfiles, 201);
    }



    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), array (
  'adm_rol_id' => 'required|foreignId',
  'user_id' => 'required|foreignId',
  'activo' => 'required|boolean',
  'descripcion' => 'required|text',
));

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $data = $request->all();

        $adm_perfiles = ADM_Perfiles::create($data);

        return response()->json($adm_perfiles, 201);
    }



    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), array (
  'adm_rol_id' => 'required|foreignId',
  'user_id' => 'required|foreignId',
  'activo' => 'required|boolean',
  'descripcion' => 'required|text',
));

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $data = $request->all();

        $adm_perfiles = ADM_Perfiles::findOrFail($id);
        $adm_perfiles->update($data);

        return response()->json($adm_perfiles, 200);
    }



    public function destroy($id)
    {
        ADM_Perfiles::findOrFail($id);
        ADM_Perfiles::destroy($id);
        return response()->json(['message' => 'Registro eliminado correctamente'], 200);
    }
    }
