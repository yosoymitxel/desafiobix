<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\ADM_Roles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ADM_RolesApiController extends Controller
{
    //




    public function index()
    {
        $adm_roles = ADM_Roles::all();
        return response()->json($adm_roles, 201);
    }



    public function show($id)
    {
        $adm_roles = ADM_Roles::findOrFail($id);
        return response()->json($adm_roles, 201);
    }



    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), array (
  'nombre' => 'required|string',
  'codigo' => 'required|string',
  'activo' => 'required|boolean',
));

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $data = $request->all();

        $adm_roles = ADM_Roles::create($data);

        return response()->json($adm_roles, 201);
    }



    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), array (
  'nombre' => 'required|string',
  'codigo' => 'required|string',
  'activo' => 'required|boolean',
));

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $data = $request->all();

        $adm_roles = ADM_Roles::findOrFail($id);
        $adm_roles->update($data);

        return response()->json($adm_roles, 200);
    }



    public function destroy($id)
    {
        ADM_Roles::findOrFail($id);
        ADM_Roles::destroy($id);
        return response()->json(['message' => 'Registro eliminado correctamente'], 200);
    }
    }
